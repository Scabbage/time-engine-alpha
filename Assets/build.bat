xcopy "E:\Unity\Time Engine\Assets\Time Engine\*" "E:\Development\TimeControl\TimeControl" /y /S

set devEnvDir=E:\Program Files (x86)\Microsoft Visual Studio\Common7\IDE

cd E:\Development\TimeControl\TimeControl\

del /Q "E:\Development\TimeControl\TimeControl\EditMode Tests"
del /Q "E:\Development\TimeControl\TimeControl\Editor"

"%devEnvDir%\devenv.exe" TimeControl.csproj /build

cp "E:\Development\TimeControl\TimeControl\bin\Debug\net35\TimeControl.dll" "E:\Unity\Time Engine Import Test\Assets\Time Engine\TimeControl.dll"
cp "E:\Unity\Time Engine\Assets\Time Engine\Editor\TimeEngineEditor.cs" "E:\Unity\Time Engine Import Test\Assets\Time Engine\Editor\TimeEngineEditor.cs"

explorer "E:\Unity\Time Engine Import Test\Assets\Time Engine"

pause