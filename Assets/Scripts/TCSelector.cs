﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TimeControl;

public class TCSelector : MonoBehaviour
{
    const float ACTIVE_THRESHOLD = 0.2f;
    const float SEGMENT_ANGLE = 60f;
    static readonly int[] controls = new int[]{ 5, 2, 4, 1, 3 };
    static readonly string[] controlLabels = new string[] { "", "◄◄ ", " ►►", " ▌▌", "●", " ▌►" };
    static readonly int[] fontSizes = new int[] { 1, 83, 83, 83, 240, 83};
    static readonly TimeState[] states = new TimeState[]
    {
        TimeState.NOMINAL,
        TimeState.REWIND,
        TimeState.FAST_FORWARD,
        TimeState.PAUSE,
        TimeState.NOMINAL, // rec isnt technically a time control
        TimeState.SLOW
    };

    public Rigidbody obj;
    public float angle;
    public TimeEngine engine;
    public Animator selectorAnim;
    public Text txt;

    int selection = -1;
    float timeout = 0f;
    bool activated = false;

	void Start ()
	{
        txt.text = "";
        SetTCMenu(false);
        selectorAnim.updateMode = AnimatorUpdateMode.UnscaledTime;
	}
	
	void Update ()
	{
        if (Input.GetButtonDown("Retry"))
        {
            activated = false;
            SetTCMenu(false);
            engine.Rewind();
        }else if(Input.GetButtonDown("TCActivate"))
        {
            activated = true;
            SetTCMenu(true);
        }

        if(Input.GetButtonUp("Retry"))
        {
            engine.Play();
        }

        if (Input.GetButtonUp("TCActivate"))
        {
            activated = false;
            SetTCMenu(false);
            txt.text = "";
            // Activate time control, if it was selected
            if(selection > 0)
            {
                engine.SetTimeState(states[selection]);
                selection = 0;
                timeout = 10f;
            }
        }
        
        if(timeout > 0f)
        {
            timeout -= Time.deltaTime;
            if (timeout <= 0f)
            {
                engine.SetTimeState(TimeState.NOMINAL);
            }
        }

        if(activated)
        {
            int newSel = GetSelection();
            if (newSel != selection)
            {
                selectorAnim.SetInteger("TC", newSel);
                selection = newSel;
                txt.text = controlLabels[selection];
                txt.fontSize = fontSizes[selection];
            }
        }
	}

    int GetSelection()
    {
        
        float x, y;
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        Vector2 vec = new Vector2(x, y);
        if(vec.magnitude < ACTIVE_THRESHOLD)
            return 0;

        float angle = (-Vector2.SignedAngle(Vector2.up, vec) + 30) % 360;
        if (angle < 0f)
            angle += 360f;

        this.angle = angle;
        for(int ii = 1; ii <= 5; ii++)
        {
            if (angle >= (SEGMENT_ANGLE * ii) && angle < (SEGMENT_ANGLE * (ii+1)))
            {
                return controls[ii-1];
            }
        }
        return 0;
    }

    void SetTCMenu(bool activated)
    {
        selectorAnim.SetBool("activated", activated);

        if(activated)
        {
            Time.timeScale = 0f;
        }else
        {
            Time.timeScale = 1f;
        }
    }
}
