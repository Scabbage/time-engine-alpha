﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VecTut : MonoBehaviour
{

    public Vector3 forward;
    public Vector3 right;
    public Vector3 up;


    void Start ()
	{
		
	}
	
	void Update ()
	{
        forward = transform.forward;
        right = transform.right;
        up = transform.up;
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(Vector3.zero, forward);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(Vector3.zero, up);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(Vector3.zero, right);

    }
}
