﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Rotater : MonoBehaviour
{
    public Quaternion rotation;
    
	void Start ()
	{
		
	}
	
	void Update ()
	{
        rotation.Normalize();
        transform.rotation = rotation;
	}
}
