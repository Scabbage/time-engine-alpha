﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TimeControl;

public class Player : MonoBehaviour
{
    public TimeEngine engine;
    public Transform observer;
    
    Rigidbody rb;
    Vector3 input;
    public float velocityV;
    float gravity = 9.81f;
    public bool isGrounded = false;
    RaycastHit rayInfo;
    public bool jump = false;
    public int jumpState = 0;

    public float moveSpeed = 0.4f;
    public float rotateSpeed = 1f;
    public float groundOffset;
    public float jumpSpeed;

	void Start ()
	{
        rb = GetComponent<Rigidbody>();
        engine.Register(transform);

	}

    private void Update()
    {
        if(Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
    }

    void FixedUpdate ()
	{
        Vector3 obsF = observer.forward;
        Vector3 obsR = observer.right;
        obsR.y = 0f;
        obsF.y = 0f;
        input = (obsR * Input.GetAxis("Horizontal")) + obsF * Input.GetAxis("Vertical");
        input *= moveSpeed;
        input.y = input.z;

        isGrounded = Physics.Raycast(transform.position, Vector3.down, out rayInfo, 2f);

        if (!isGrounded)
        {
            if (jumpState == 1)
                jumpState = 2;
            velocityV -= gravity * Time.fixedDeltaTime;
            rb.MovePosition(rb.position + new Vector3(input.x, velocityV * Time.fixedDeltaTime, input.y));
        }
        else
        {
            if (jumpState == 2)
            {
                jumpState = 0;
                velocityV = 0f;
            }else
            {
                if (jump)
                {
                    velocityV = jumpSpeed;
                    jumpState = 1;
                } else
                {
                    if (jumpState != 1)
                        velocityV = 0f;
                }
            }
            rb.MovePosition(rb.position + new Vector3(input.x, rayInfo.point.y - rb.position.y + groundOffset + velocityV * Time.fixedDeltaTime, input.y));

        }

        if (input.magnitude > 0.1f)
        {
            rb.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(new Vector3(input.x, 0f, input.y), Vector3.up), Time.deltaTime * rotateSpeed);
        }

        jump = false;
    }
}
