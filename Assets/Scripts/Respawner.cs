﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour
{
    public float spawnDelay = 5f;
    public GameObject objectToRespawn;

    float timer = 0f;

	void Start ()
	{
		
	}
	
	void Update ()
	{
        timer -= Time.deltaTime;
        if(timer <= 0f)
        {
            timer = spawnDelay;
            objectToRespawn.transform.position = transform.position;
        }
	}
}
