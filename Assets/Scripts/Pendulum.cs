﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TimeControl;


public class Pendulum : MonoBehaviour
{
    public TimeEngine engine;
    public float period;
    public float theta = 20f;

    public float moveSpeed = 1f;
    public float time = 0f;

    [Recorded]
    public float AnimTime
    {
        get
        {
            return time;
        }

        set
        {
            time = value;
            transform.localRotation = Quaternion.Euler(0f, 0f, theta * Mathf.Sin(2f * Mathf.PI * value / period));
        }
    }


	void Start ()
	{
        engine.Register(this);

	}
	
    public void OnTimeStateChanged(TimeState newState)
    {
        switch (newState)
        {
            case TimeState.NOMINAL:
                moveSpeed = 1f;
                break;
            case TimeState.PLAYBACK:
            case TimeState.REWIND:
            case TimeState.PAUSE:
                moveSpeed = 0f;
                break;
            case TimeState.SLOW:
                moveSpeed = 0.5f;
                break;
            case TimeState.FAST_FORWARD:
                moveSpeed = 2f;
                break;

        }
    }

	void Update ()
	{
        AnimTime = time + Time.deltaTime * moveSpeed;

    }


}
