﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace TimeControl
{
    public static class Serialiser
    {
        enum SType {BYTE, SHORT, USHORT, INT, UINT, LONG, ULONG, FLOAT, DOUBLE, DECIMAL, VEC3, QUAT }

        private static Dictionary<Type, SType> serialisationMap = CreateSerialiserDictionary();

        private static Dictionary<Type, SType> CreateSerialiserDictionary()
        {
            var d = new Dictionary<Type, SType>();
            d.Add(typeof(byte),       SType.BYTE);
            d.Add(typeof(short),      SType.SHORT);
            d.Add(typeof(ushort),     SType.USHORT);
            d.Add(typeof(int),        SType.INT);
            d.Add(typeof(uint),       SType.UINT);
            d.Add(typeof(long),       SType.LONG);
            d.Add(typeof(ulong),      SType.ULONG);
            d.Add(typeof(float),      SType.FLOAT);
            d.Add(typeof(double),     SType.DOUBLE);
            d.Add(typeof(decimal),    SType.DECIMAL);
            d.Add(typeof(Vector3),    SType.VEC3);
            d.Add(typeof(Quaternion), SType.QUAT);
            return d;
        }

        public static byte[] GetBytes(object o)
        {
            Type t = o.GetType();
            SType stype;
            if(serialisationMap.TryGetValue(t, out stype))
            {
                switch (stype)
                {
                    case SType.BYTE:
                        return BitConverter.GetBytes((byte)o);
                    case SType.SHORT:
                        return BitConverter.GetBytes((short)o);
                    case SType.USHORT:
                        return BitConverter.GetBytes((ushort)o);
                    case SType.INT:
                        return BitConverter.GetBytes((int)o);
                    case SType.UINT:
                        return BitConverter.GetBytes((uint)o);
                    case SType.LONG:
                        return BitConverter.GetBytes((long)o);
                    case SType.ULONG:
                        return BitConverter.GetBytes((ulong)o);
                    case SType.FLOAT:
                        return BitConverter.GetBytes((float)o);
                    case SType.DOUBLE:
                        return BitConverter.GetBytes((double)o);
                    case SType.DECIMAL:
                        return GetBytes((decimal)o);
                    case SType.VEC3:
                        return GetBytes((Vector3)o);
                    case SType.QUAT:
                        return GetBytes((Quaternion)o);
                }
            }
            
            return null;
        }
        
        



        public static object Deserialise(byte[] bytes, int startIndex, Type t)
        {
            int size;
            return Deserialise(bytes, startIndex, t, out size);
        }

        public static object Deserialise(byte[] bytes, int startIndex, Type t, out int size)
        {
            if (t == typeof(byte))
            {
                size = sizeof(byte);
                return bytes[startIndex];
            }
            else if (t == typeof(short))
            {
                size = sizeof(short);
                return ToShort(bytes, startIndex);
            }
            else if (t == typeof(ushort))
            {
                size = sizeof(ushort);
                return ToUShort(bytes, startIndex);
            }
            else if (t == typeof(int))
            {
                size = sizeof(int);
                return ToInt(bytes, startIndex);
            }
            else if (t == typeof(uint))
            {
                size = sizeof(uint);
                return ToUInt(bytes, startIndex);
            }
            else if (t == typeof(long))
            {
                size = sizeof(long);
                return ToLong(bytes, startIndex);
            }
            else if (t == typeof(ulong))
            {
                size = sizeof(ulong);
                return ToULong(bytes, startIndex);
            }
            else if (t == typeof(float))
            {
                size = sizeof(float);
                return ToFloat(bytes, startIndex);
            }
            else if (t == typeof(double))
            {
                size = sizeof(double);
                return ToDouble(bytes, startIndex);
            }
            else if (t == typeof(decimal))
            {
                size = sizeof(decimal);
                return ToDecimal(bytes, startIndex);
            }
            else if (t == typeof(Vector3))
            {
                size = 3 * sizeof(float);
                return ToVector3(bytes, startIndex);
            }
            else if (t == typeof(Quaternion))
            {
                size = 4 * sizeof(float);
                return ToQuaternion(bytes, startIndex);
            }
            size = 0;
            return null;
        }

        public static int ToInt(byte[] bytes, int startIndex)
        {
            return BitConverter.ToInt32(bytes, startIndex);
        }

        public static uint ToUInt(byte[] bytes, int startIndex)
        {
            return BitConverter.ToUInt32(bytes, startIndex);
        }
        public static short ToShort(byte[] bytes, int startIndex)
        {
            return BitConverter.ToInt16(bytes, startIndex);
        }
        public static ushort ToUShort(byte[] bytes, int startIndex)
        {
            return BitConverter.ToUInt16(bytes, startIndex);
        }
        public static long ToLong(byte[] bytes, int startIndex)
        {
            return BitConverter.ToInt64(bytes, startIndex);
        }
        public static ulong ToULong(byte[] bytes, int startIndex)
        {
            return BitConverter.ToUInt64(bytes, startIndex);
        }
        public static float ToFloat(byte[] bytes, int startIndex)
        {
            return BitConverter.ToSingle(bytes, startIndex);
        }
        public static double ToDouble(byte[] bytes, int startIndex)
        {
            return BitConverter.ToDouble(bytes, startIndex);
        }
        public static Vector3 ToVector3(byte[] bytes, int startIndex)
        {
            return new Vector3(
                    BitConverter.ToSingle(bytes, startIndex),
                    BitConverter.ToSingle(bytes, startIndex + 4),
                    BitConverter.ToSingle(bytes, startIndex + 8)
                );
        }
        public static Quaternion ToQuaternion(byte[] bytes, int startIndex)
        {
            return new Quaternion(
                    BitConverter.ToSingle(bytes, startIndex),
                    BitConverter.ToSingle(bytes, startIndex + 4),
                    BitConverter.ToSingle(bytes, startIndex + 8),
                    BitConverter.ToSingle(bytes, startIndex + 12)
                );
        }
        public static decimal ToDecimal(byte[] bytes, int startIndex)
        {
            int[] bits = new int[4];

            for (int ii = 0; ii < 4; ii++)
            {
                bits[ii + startIndex] = BitConverter.ToInt32(bytes, ii * 4);
            }

            return new decimal(bits);
        }
        

        public static byte[] GetBytes(byte b)
        {
            return new byte[] { b }; // that was easy
        }

        public static byte[] GetBytes(sbyte sb)
        {
            return BitConverter.GetBytes(sb);
        }

        public static byte[] GetBytes(short s)
        {
            return BitConverter.GetBytes(s);
        }

        public static byte[] GetBytes(ushort us)
        {
            return BitConverter.GetBytes(us);
        }

        public static byte[] GetBytes(int i)
        {
            return BitConverter.GetBytes(i);
        }

        public static byte[] GetBytes(uint ui)
        {
            return BitConverter.GetBytes(ui);
        }

        public static byte[] GetBytes(float f)
        {
            return BitConverter.GetBytes(f);
        }

        public static byte[] GetBytes(double d)
        {
            return BitConverter.GetBytes(d);
        }

        public static byte[] GetBytes(long l)
        {
            return BitConverter.GetBytes(l);
        }

        public static byte[] GetBytes(ulong ul)
        {
            return BitConverter.GetBytes(ul);
        }


        public static byte[] GetBytes(decimal dec)
        {
            int[] bits = decimal.GetBits(dec);
            byte[] bytes = new byte[16];
            byte[] temp;
            for (int ii = 0; ii < 4; ii++)
            {
                temp = BitConverter.GetBytes(bits[ii]);
                bytes[ii * 4] = temp[0];
                bytes[ii * 4 + 1] = temp[1];
                bytes[ii * 4 + 2] = temp[2];
                bytes[ii * 4 + 3] = temp[3];
            }
            return bytes;
        }

        public static byte[] GetBytes(Vector3 v)
        {
            byte[] bytes = new byte[12];
            byte[] temp;

            for (int ii = 0; ii < 3; ii++)
            {
                temp = System.BitConverter.GetBytes(v[ii]);
                bytes[ii * 4] = temp[0];
                bytes[ii * 4 + 1] = temp[1];
                bytes[ii * 4 + 2] = temp[2];
                bytes[ii * 4 + 3] = temp[3];
            }
            return bytes;
        }

        public static byte[] GetBytes(Quaternion q)
        {
            byte[] bytes = new byte[16];
            byte[] temp;

            for (int ii = 0; ii < 4; ii++)
            {
                temp = System.BitConverter.GetBytes(q[ii]);
                bytes[ii * 4] = temp[0];
                bytes[ii * 4 + 1] = temp[1];
                bytes[ii * 4 + 2] = temp[2];
                bytes[ii * 4 + 3] = temp[3];
            }
            return bytes;
        }

        
    }
}