﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections.Generic;
using TimeControl;


public class SampleConverterTest {

    [Test]
    public void SampleConverterTestSimplePasses()
    {
        List<byte[][]> samples = new List<byte[][]>();
        byte[][] sample;
        int uuid = 2397634;
        int convertedUUID;
        sample = new byte[][] {
            ToBytes(0, 1, 2, 3, 4, 5),
            ToBytes(0, 1, 2, 3, 4, 5),
            ToBytes(0, 1, 2, 3, 4, 5),
            ToBytes(255, 254, 253, 252, 251, 250)
        };

        samples.Add(sample);

        byte[] serialisedSample = SampleBitConverter.SampleToBytes(uuid, samples);

        List<byte[][]> convertedSample = SampleBitConverter.BytesToSample(serialisedSample, out convertedUUID);

        for(int ii = 0; ii < samples[0].Length; ii++)
        {
            for(int jj = 0; jj < samples[0][ii].Length; jj++)
            {
                Assert.AreEqual(samples[0][ii][jj], convertedSample[0][ii][jj]);
            }
        }
    }

    byte[] ToBytes(params byte[] nums)
    {
        return nums;
    }
}
