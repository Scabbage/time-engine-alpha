﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TimeControl
{
    public class BehaviourRigidbody : TimeBehaviour<Rigidbody>
    {
        private class RigidbodyMetadata
        {
            public bool useGravity;
            public float drag;
            public float angularDrag;
            public RigidbodyConstraints constraints;
            public float lastTimeFlow = 1f;
            public Vector3 gravityMod;
            public Vector3 pauseVelocity;
        }

        static Dictionary<Rigidbody, RigidbodyMetadata> bodies = new Dictionary<Rigidbody, RigidbodyMetadata>();

        public override void OnRegister(Rigidbody rb)
        {
            RigidbodyMetadata meta;
            if(!bodies.TryGetValue(rb, out meta))
            {
                meta = new RigidbodyMetadata();
                bodies.Add(rb, meta);
                meta.constraints = rb.constraints;
                meta.useGravity = rb.useGravity;
                meta.drag = rb.drag;
                meta.angularDrag = rb.angularDrag;
            }
        }

        public override void OnStateChanged(TimeState newState, TimeState oldState, Rigidbody rb)
        {
            RigidbodyMetadata meta;
            if (bodies.TryGetValue(rb, out meta))
            {
                switch (newState)
                {
                    case TimeState.NOMINAL:
                        rb.constraints = meta.constraints;
                        rb.useGravity = meta.useGravity;
                        rb.drag = meta.drag;
                        rb.angularDrag = meta.angularDrag;

                        meta.gravityMod = Vector3.zero;
                        break;
                    case TimeState.PAUSE:
                        rb.constraints = RigidbodyConstraints.FreezeAll;
                        rb.useGravity = false;
                        rb.drag = meta.drag;
                        rb.angularDrag = meta.angularDrag;

                        meta.gravityMod = Vector3.zero;
                        meta.pauseVelocity = rb.velocity;
                        meta.lastTimeFlow = 0f;
                        break;
                    case TimeState.REWIND:
                        rb.constraints = meta.constraints;
                        rb.useGravity = false;
                        meta.gravityMod = Vector3.zero;
                        rb.drag = meta.drag;
                        if (oldState == TimeState.PAUSE)
                            rb.velocity = meta.pauseVelocity;
                        else
                            rb.velocity *= 1f / meta.lastTimeFlow;
                        meta.lastTimeFlow = 1f;
                        break;
                    case TimeState.FAST_FORWARD:
                        rb.constraints = meta.constraints;
                        rb.useGravity = false;
                        meta.gravityMod = 4f * Physics.gravity;
                        rb.angularDrag = meta.angularDrag / 2f;
                        rb.drag = 0.5f * meta.drag;
                        if (oldState == TimeState.PAUSE)
                            rb.velocity = meta.pauseVelocity;
                        else
                            rb.velocity *= 2f / meta.lastTimeFlow;
                        meta.lastTimeFlow = 2f;
                        break;
                    case TimeState.SLOW:
                        rb.constraints = meta.constraints;
                        rb.useGravity = false;
                        meta.gravityMod = 0.25f * Physics.gravity;
                        rb.angularDrag = 2f * meta.angularDrag;
                        rb.drag = 2f * meta.drag;
                        if (meta.lastTimeFlow == 0f)
                            rb.velocity = 0.5f * meta.pauseVelocity;
                        else
                            rb.velocity *= 0.5f / meta.lastTimeFlow;

                        meta.lastTimeFlow = 1f;
                        break;

                }
            }
        }

        public override void OnFixedUpdate(Rigidbody rb)
        {
            RigidbodyMetadata meta;
            if(bodies.TryGetValue(rb, out meta))
            {
                if (meta.gravityMod.y != 0f)
                {
                    rb.AddForce(meta.gravityMod);
                }
            }
        }
    }
}