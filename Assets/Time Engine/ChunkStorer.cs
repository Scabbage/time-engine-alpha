﻿#define DEBUG_OUTPUT

using UnityEngine;
using System.IO;
using System.Threading;
using System.IO.Compression;


public static class ChunkStorer
{
    private static string storagePath = Path.Combine(Application.persistentDataPath, "Data/");
    
    /// <summary>
    /// Compresses a given byte[] and stores it on disk asynchronously.
    /// </summary>
    /// <param name="bytes"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public static Thread StoreChunkAsync(byte[] bytes, int id)
    {
        Thread t = new Thread(() => StoreChunk(bytes, id));
        t.Start();
        return t;
    }

    /// <summary>
    /// Compresses a given byte[] and stores it on disk.
    /// </summary>
    /// <param name="bytes"></param>
    /// <param name="id"></param>
    public static void StoreChunk(byte[] bytes, int id)
    {
        string path = storagePath;
        string fileName = "chunk" + id ;
        byte[] compressedBytes;
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        compressedBytes = Compress(bytes);
#if DEBUG_OUTPUT
        Debug.Log("Writing chunk of " + bytes.Length + " (" + compressedBytes.Length + " compressed). Compression ratio: " + ((float)compressedBytes.Length / bytes.Length));
#endif
        File.WriteAllBytes(Path.Combine(path, fileName), compressedBytes);
    }

    public static byte[] GetChunk(int id)
    {
        string path = storagePath + "chunk" + id;

        return File.ReadAllBytes(path);
    }

    public static byte[] Compress(byte[] original)
    {
        MemoryStream output = new MemoryStream();
        using (DeflateStream dstream = new DeflateStream(output, CompressionMode.Compress))
        {
            dstream.Write(original, 0, original.Length);
        }
        return output.ToArray();
    }

    public static byte[] Decompress(byte[] data)
    {
        MemoryStream input = new MemoryStream(data);
        MemoryStream output = new MemoryStream();
        using (DeflateStream dstream = new DeflateStream(input, CompressionMode.Decompress))
        {
            dstream.CopyTo(output);
        }
        return output.ToArray();
    }

    public static void CopyTo(this Stream source, Stream destination, int bufferSize = 81920)
    {
        byte[] array = new byte[bufferSize];
        int count;
        while ((count = source.Read(array, 0, array.Length)) != 0)
        {
            destination.Write(array, 0, count);
        }
    }
}
