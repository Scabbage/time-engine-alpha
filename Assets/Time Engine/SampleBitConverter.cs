﻿#define DEBUG_OUTPUT

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Converts sample buffers to and from byte arrays.
/// </summary>
public static class SampleBitConverter
{
    public static byte[] SampleToBytes(int uuid, IEnumerable<byte[][]> sampleBuffer)
    {
        int dbgSampleCount = 0, dbgFieldCount = 0;

        int currentSampleIdx = 0;
        byte[] temp = new byte[4];
        List<byte> bytes = new List<byte>();

        // First 4 bytes are the number of samples (not yet known)
        bytes.AddRange(temp);

        // next 4 are the uuid
        temp = BitConverter.GetBytes(uuid);
        bytes.AddRange(temp);

        foreach(byte[][] sample in sampleBuffer)
        {
            dbgSampleCount++;

            // Next 4 bytes are the size of the sample
            temp = BitConverter.GetBytes(sample.Length);
            bytes.AddRange(temp);

            foreach(byte[] field in sample)
            {
                dbgFieldCount++;
                // Next 4 are the field length
                temp = BitConverter.GetBytes(field.Length);
                bytes.AddRange(temp);

                // And finally the field itself:
                bytes.AddRange(field);
            }
            currentSampleIdx++;
        }

        temp = BitConverter.GetBytes(currentSampleIdx);

        for (int ii = 0; ii < 4; ii++)
            bytes[ii] = temp[ii];

#if DEBUG_OUTPUT
        Debug.Log("Converted " + dbgSampleCount + " samples with " + dbgFieldCount + " fields to a serialised array of length " + bytes.Count);
#endif
        return bytes.ToArray();
    }

    public static List<byte[][]> BytesToSample(byte[] bytes, out int uuid)
    {
        List<byte[][]> result = new List<byte[][]>();
        int bufferLength;
        int countedSamples = 0;
        int sampleLength;
        int currentIdx = 8;
        byte[][] convertedBytes;

        bufferLength = BitConverter.ToInt32(bytes, 0);
        uuid = BitConverter.ToInt32(bytes, 4);

        while(currentIdx < bytes.Length)
        {
            // Current id is at the length of this sample
            sampleLength = BitConverter.ToInt32(bytes, currentIdx);
            currentIdx += 4;
            convertedBytes = new byte[sampleLength][];
            for(int ii = 0; ii < sampleLength; ii++)
            {
                // Next 4 bytes are the field length
                convertedBytes[ii] = new byte[BitConverter.ToInt32(bytes, currentIdx)];
                currentIdx += 4;

                // Copy in the field
                for(int ff = 0; ff < convertedBytes[ii].Length; ff++)
                {
                    convertedBytes[ii][ff] = bytes[currentIdx];
                    currentIdx++;
                }
            }

            // Now add the sample:
            result.Add(convertedBytes);
            countedSamples++;
        }
#if DEBUG_OUTPUT
        if(countedSamples != bufferLength)
        {
            Debug.LogWarning("Number of samples during deserialisation did not match. Given: " + bufferLength + " but converted " + countedSamples + " samples.");
        }
#endif
        return result;
    }
}
