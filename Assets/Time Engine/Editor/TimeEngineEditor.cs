﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;


namespace TimeControl
{
#if UNITY_EDITOR
    [CustomEditor(typeof(TimeEngine))]
    public class TimeEngineEditor : Editor
    {

        TimeState oldTS;
        TimeState newTS;

        TimeEngine.RecordingState oldRS;
        TimeEngine.RecordingState newRS;

        TimeEngine.TEBufferSize bufSize
        {
            get
            {
                return TimeEngine.DefaultBufferSize;
            }
            set
            {
                TimeEngine.DefaultBufferSize = value;
            }
        }

        TimeEngine eng;

        int repaintTrigger = 10;
        int repaintCounter = 0;

        int availableMemoryTrigger = 200;
        int availableMemoryCounter = 0;

        long availableMemory;
        int gcCollects;
        
        void OnEnable()
        {
            eng = target as TimeEngine;
            EditorApplication.update += Update;
        }

        void Update()
        {
            if (repaintCounter-- <= 0)
            {
                repaintCounter = repaintTrigger;
                Repaint();
            }
            if(availableMemoryCounter-- <= 0)
            {
                availableMemoryCounter = availableMemoryTrigger;
                availableMemory = System.GC.GetTotalMemory(false);
                gcCollects = 0;
                for(int ii = 0; ii < System.GC.MaxGeneration; ii++)
                    gcCollects += System.GC.CollectionCount(ii);
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            TimeSpan span = eng.Timecode;
            
            LabelField(span.Minutes.ToString("D2") + ":" + span.Seconds.ToString("D2") + ":" + span.Milliseconds.ToString("D3"));

            newTS = (TimeState)EditorGUILayout.EnumPopup("Global Time State", oldTS);
            EditorGUILayout.Space();

            newRS = (TimeEngine.RecordingState)EditorGUILayout.EnumPopup("Record State", eng.recordingState);
            EditorGUILayout.Space();
            if (!Application.isPlaying)
            {
                bufSize = (TimeEngine.TEBufferSize)EditorGUILayout.EnumPopup("Buffer Capacity", bufSize);
                if(bufSize == TimeEngine.TEBufferSize.FIVE_MINUTES || bufSize == TimeEngine.TEBufferSize.TEN_MINUTES)
                {
                    EditorGUILayout.HelpBox("Larger buffer sizes may cause unacceptably high memory consumption.", MessageType.Warning);
                }
                EditorGUILayout.Space();
            }else
            {
                LabelField("Buffer Capacity: " + bufSize);
                EditorGUILayout.Space();
            }
            Rect r = GUILayoutUtility.GetRect(18, 18, "TextField");
            float progress = eng.CurrentSample / (float)eng.MaxSamples;
            EditorGUI.ProgressBar(r, progress, "Buffer Capacity Used");
            if(progress > 0.95f)
            {
                EditorGUILayout.HelpBox("Buffer size approaching maximum capacity. A storage write will be triggered soon.", MessageType.Warning);
            }
            LabelField("Buffer Memory Usage: " + eng.MemoryUsage.ToSize(ByteUnit.KB) + " KB");
            LabelField("GC Memory Alloc: " + availableMemory.ToSize(ByteUnit.MB) + " MB");

            SerializedProperty prop = serializedObject.FindProperty("addOnStart");
            if (prop != null)
                EditorGUILayout.PropertyField(prop, new GUIContent("Automatically Added Objects"), true);
            else
                EditorGUILayout.HelpBox("Couldn't find property.", MessageType.Error);

            if (oldTS != newTS)
            {
                eng.SetTimeState(newTS);
                oldTS = newTS;
            }

            if(eng.recordingState != newRS)
            {
                eng.recordingState = newRS;
            }

            if (bufSize != TimeEngine.DefaultBufferSize)
            {
                Debug.Log("Updating buffer capacity from " + TimeEngine.DefaultBufferSize + " to " + bufSize);
                eng.SetBufferCapacity(bufSize);
            }

            serializedObject.ApplyModifiedProperties();
        }

        void LabelField(string text)
        {
            Rect r = GUILayoutUtility.GetRect(18, 18, "TextField");
            EditorGUI.LabelField(r, text);
        }
    }
#endif
}