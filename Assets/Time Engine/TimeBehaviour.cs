﻿using System;

namespace TimeControl
{
    public interface TimeBehaviour
    {
        Type EntityType { get; }
        void OnRegister(object o);
        void OnFixedUpdate(object o);
        void OnStateChanged(TimeState newState, TimeState oldState, object o);
    }

    public abstract class TimeBehaviour<T> : TimeBehaviour where T : class
    {
        public Type EntityType
        {
            get { return typeof(T); }
        }

        public void OnRegister(object o)
        {
            T cast = o as T;
            if (cast != null)
                OnRegister(cast);
            else
                throw new ArgumentException("Object " + o + " has invalid type. Expected: " + typeof(T) + ", given: " + o.GetType());
        }

        public void OnFixedUpdate(object o)
        {
            T cast = o as T;
            if (cast != null)
                OnFixedUpdate(cast);
            else
                throw new ArgumentException("Object " + o + " has invalid type. Expected: " + typeof(T) + ", given: " + o.GetType());
        }

        public void OnStateChanged(TimeState newState, TimeState oldState, object o)
        {
            T cast = o as T;
            if (cast != null)
                OnStateChanged(newState, oldState, cast);
            else
                throw new ArgumentException("Object " + o + " has invalid type. Expected: " + typeof(T) + ", given: " + o.GetType());
        }

        public abstract void OnRegister(T obj);
        public abstract void OnFixedUpdate(T obj);
        public abstract void OnStateChanged(TimeState newState, TimeState oldState, T obj);
    }
}