﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SampleBuffer<T>
{
    public bool allowOverCapacity = false;

    int size;
    List<T> primarySamples;
    List<T> secondarySamples;

    public int Count
    {
        get
        {
            return primarySamples.Count;
        }
    }

    public int Capacity
    {
        get { return size; }
    }


    public IEnumerable<T> SecondaryBuffer
    {
        get { return secondarySamples; }
    }

    public SampleBuffer(int size)
    {
        this.size = size;
        primarySamples = new List<T>(size);
        secondarySamples = new List<T>(size);
    }

    public void Add(T sample)
    {
        if (primarySamples.Count >= size && !allowOverCapacity)
        {
            throw new ArgumentException("Sample buffer at maximum capacity: " + primarySamples.Count);
        }
        else
        {
            primarySamples.Add(sample);
        }
    }

    

    /// <summary>
    /// O(1) Switches the primary and secondary buffers.
    /// </summary>
    public void SwitchBuffers()
    {
        // Switch the buffers
        var temp = primarySamples;
        primarySamples = secondarySamples;
        secondarySamples = temp;
    }

    /// <summary>
    /// Clears the primary buffer.
    /// </summary>
    public void ClearPrimary()
    {
        primarySamples.Clear();
    }

    /// <summary>
    /// Shifts the primary buffer into the secondary. The primary will be empty.
    /// </summary>
    public void RightShift()
    {
        SwitchBuffers();
        ClearPrimary();
    }

    /// <summary>
    /// Shifts the secondary buffer into the primary. The given array will become the secondary.
    /// </summary>
    public void LeftShift(List<T> newSecondary)
    {
        if (newSecondary == null)
            throw new ArgumentException("New list given was null.");

        primarySamples = secondarySamples;
        secondarySamples = newSecondary;
    }

    /// <summary>
    /// Removes the last sample from the buffer and returns it.
    /// Returns null if the buffer is empty.
    /// </summary>
    /// <returns></returns>
    public T RemoveLast()
    {
        if (primarySamples.Count > 0)
        {
            T res = primarySamples[primarySamples.Count - 1];
            primarySamples.RemoveAt(primarySamples.Count - 1);
            return res;
        }
        return default(T);
    }

    public T this[int idx]
    {
        get
        {
            return primarySamples[idx];
        }
        set
        {
            primarySamples[idx] = value;
        }
    }
}
