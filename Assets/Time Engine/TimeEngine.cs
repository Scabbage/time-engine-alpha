﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.ComponentModel;

namespace TimeControl
{
    public enum TimeState
    {
        NOMINAL,
        REWIND,
        FAST_FORWARD,
        PAUSE,
        SLOW,
        PLAYBACK
    }
    public delegate void OnTimeStateChanged(TimeState newState, TimeState oldState);


    /// <summary>
    /// Records the state of all registered objects every frame.
    /// </summary>
    [AddComponentMenu("Time Engine")]
    public class TimeEngine : MonoBehaviour
    {
        private class RecordableMetadata
        {
            public int jumpFrameIdx;
            public TimeState state;
            public readonly int id;

            public RecordableMetadata(int id, TimeState state)
            {
                jumpFrameIdx = 0;
                this.id = id;
                this.state = state;
            }
        }

        /// <summary>
        /// The latest initialised TimeEngine instance.
        /// </summary>
        public static TimeEngine main { get; private set; }

        /// <summary>
        /// The default value for the sample buffer.
        /// At 50Hz, 3000 is equivalent to one minute of recording data.
        /// </summary>
        const TEBufferSize DEFAULT_MAX_SAMPLES = TEBufferSize.ONE_MINUTE;

        public UnityEngine.Object[] addOnStart;
        
        public enum RecordingState { PAUSED, RECORDING, REWINDING }

        public enum TEBufferSize
        {
            [Description("30 Seconds")]
            THIRTY_SECONDS = 30,
            [Description("1 Minute")]
            ONE_MINUTE = 60,
            [Description("2 Minutes")]
            TWO_MINUTES = 120,
            [Description("5 Minutes")]
            FIVE_MINUTES = 300,
            [Description("10 Minutes")]
            TEN_MINUTES = 600
        }
        
        public static TEBufferSize DefaultBufferSize
        {
            get
            {
                int result = PlayerPrefs.GetInt("TimeEngine_DefaultBufferSize");
                if (result == 0)
                {
                    result = (int)TEBufferSize.ONE_MINUTE;
                    PlayerPrefs.SetInt("TimeEngine_DefaultBufferSize", result);
                }
                return (TEBufferSize)result;
            }
            set
            {
                PlayerPrefs.SetInt("TimeEngine_DefaultBufferSize", (int)value);
            }
        }

        public TimeSpan Timecode
        {
            get
            {
                long tickCount = absoluteSampleIdx;
                long step = (long)1e+7 / frequency;
                tickCount *= step;
                TimeSpan result = new TimeSpan(tickCount);
                return result;
            }
        }

        /// <summary>
        /// The update frequency, in Hz.
        /// </summary>
        static int frequency = 50;

        int absoluteSampleIdx = 0;

        /// <summary>
        /// All of the registered custom behavioural systems.
        /// </summary>
        Dictionary<object, List<TimeBehaviour>> behaviourSystems = new Dictionary<object, List<TimeBehaviour>>();

        /// <summary>
        /// The definitions for all recorded types.
        /// </summary>
        Dictionary<Type, StateDefinition> definitions = new Dictionary<Type, StateDefinition>();

        /// <summary>
        /// Maps the recorded objects with their current state.
        /// </summary>
        Dictionary<object, RecordableMetadata> registeredObjects = new Dictionary<object, RecordableMetadata>();

        /// <summary>
        /// A list of samples. The last sample is the most recent.
        /// </summary>
        SampleBuffer<byte[][]> samples;

        /// <summary>
        /// The next free ID to use if another object is registered.
        /// Corresponds to the index of the objects data in a sample.
        /// </summary>
        int nextObjID = 1; // initialised to 1, as 0 is the sample header

        /// <summary>
        /// The index of the currently used chunk.
        /// </summary>
        int currentChunkID = 0;

        /// <summary>
        /// Returns the index of the last sample added.
        /// </summary>
        public int CurrentSample
        {
            get { return samples.Count - 1; }
        }

        /// <summary>
        /// The current state of recording.
        /// When paused, no samples will be recorded.
        /// When playing, samples from all objects are recorded.
        /// When rewinding, all objects will have their state set to the latest sample, and the sample will be deleted.
        /// </summary>
        public RecordingState recordingState { get; set; }

        /// <summary>
        /// The maximum samples that can be stored in the sample buffer until a
        /// storage write is triggered.
        /// </summary>
        public int MaxSamples { get; private set; }

        /// <summary>
        /// The size, in bytes, of the current sample buffer.
        /// </summary>
        public int MemoryUsage { get; private set; }

        int CurrentChunkFileIdentifier { get { return GetHashCode() + currentChunkID; } }

        public TimeEngine()
        {
            main = this;
            
            samples = new SampleBuffer<byte[][]>(frequency * (int)DEFAULT_MAX_SAMPLES);

            // Create default definitions for Unity types
            StateDefinition def;

            def = StateDefinitionFactory.CreateDefinition(typeof(Rigidbody), "position", "rotation", "velocity", "angularVelocity");
            definitions.Add(typeof(Rigidbody), def);

            def = StateDefinitionFactory.CreateDefinition(typeof(Transform), "position", "rotation", "localScale");
            definitions.Add(typeof(Transform), def);
            
            def = StateDefinitionFactory.CreateDefinition(typeof(RectTransform), "position", "rotation", "localScale");
            definitions.Add(typeof(RectTransform), def);

            // Create the custom behaviours for Unity types

            AddSystem(new BehaviourRigidbody());

            Play();
        }

        /// <summary>
        /// Registers the given object.
        /// Returns true if it was registered, false if it was already registered.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public bool Register(object o)
        {
            if (o.Equals(null))
            {
                Debug.LogError("Object given is null.");
                return false;
            }

            if(!registeredObjects.ContainsKey(o))
            {
                // Create a definition for this type, if it doesn't exist.
                if (!definitions.ContainsKey(o.GetType()))
                {
                    StateDefinition sd = StateDefinitionFactory.CreateDefinition(o.GetType());
                    definitions.Add(o.GetType(), sd);
                }
                // Call the OnRegister callback for all behaviour systems on this type
                List<TimeBehaviour> behaviours;
                if (behaviourSystems.TryGetValue(o.GetType(), out behaviours))
                {
                    foreach(TimeBehaviour behaviour in behaviours)
                    {
                        behaviour.OnRegister(o);
                    }
                }

                registeredObjects.Add(o, new RecordableMetadata(nextObjID, TimeState.NOMINAL));
                nextObjID++;
                //print("Registered object " + o.ToString() + ". Hashcode: " + o.GetHashCode());
                return true;
            }else
            {
                print("Dictionary contains key " + o + ": " + registeredObjects.ContainsKey(o));
            }
            return false;
        }

        public bool Register(IEnumerable objs)
        {
            bool result = true;
            foreach(object o in objs)
            {
                result &= Register(o);
            }
            return result;
        }
        
        public bool Register(GameObject go)
        {
            bool result = false;
            result |= RegisterComponent<Rigidbody>(go);
            result |= RegisterComponent<Rigidbody2D>(go);
            if (!result)
            {
                if (!RegisterComponent<Transform>(go))
                    RegisterComponent<RectTransform>(go);
            }
            return result;
        }

        bool RegisterComponent<T>(GameObject parent)
        {
            T c = parent.GetComponent<T>();
            if (!c.Equals(null))
                return Register(c);
            return false;
        }

        private void Start()
        {
            MaxSamples = TimeToSampleCount(DefaultBufferSize);
            samples = new SampleBuffer<byte[][]>(MaxSamples);

            
            foreach (UnityEngine.Object obj in addOnStart)
            {
                Type t = obj.GetType();

                if (t == typeof(GameObject))
                {
                    Register((GameObject)obj);
                }else if(t == typeof(Rigidbody))
                {
                    Register(obj);
                }else if(t == typeof(Rigidbody2D))
                {
                    Register(obj);
                }
            }
        }

        /// <summary>
        /// Adds a definition for a type, defining which properties will be recorded.
        /// </summary>
        /// <param name="s"></param>
        public void AddDefinition(StateDefinition s)
        {
            if (!definitions.ContainsKey(s.Type))
            {
                definitions.Add(s.Type, s);
            }else
            {
                throw new InvalidOperationException("Cannot add new definition for type '" + s.Type + "', as it already exists.");
            }
        }
        
        public void AddSystem(TimeBehaviour system)
        {
            List<TimeBehaviour> systems;
            if(!behaviourSystems.TryGetValue(system.EntityType, out systems))
            {
                // The given type has no registered systems at all. Create a list for it:
                systems = new List<TimeBehaviour>();
                behaviourSystems.Add(system.EntityType, systems);
            }
            //Debug.Log("Added new system " + system.ToString() + " to entity type " + system.EntityType);
            // Add the new system
            systems.Add(system);
        }

        public void FixedUpdate()
        {
            // Pause: simply early out
            // Play: set the state of any objects that need it, record the current state and store it
            // Rewind: set the state of ALL objects to the latest sample before discarding it
            if (recordingState != RecordingState.PAUSED)
            {
                if (recordingState == RecordingState.RECORDING)
                {
                    FixedPlayUpdate();
                } else if (recordingState == RecordingState.REWINDING)
                {
                    FixedRewindUpdate();
                }
            }
        }

        /// <summary>
        /// Stops recording. Rewinds all objects until Play() is called or the sample list is exhausted.
        /// Note that this affects all objects globally, and any state stored during
        /// the rewind will be wiped.
        /// </summary>
        public void Rewind()
        {
            recordingState = RecordingState.REWINDING;
        }

        /// <summary>
        /// Stops recording, if it is.
        /// </summary>
        public void Pause()
        {
            recordingState = RecordingState.PAUSED;
        }

        /// <summary>
        /// Resumes recording, if it wasn't already.
        /// </summary>
        public void Play()
        {
            recordingState = RecordingState.RECORDING;
        }

        public void SetBufferCapacity(TEBufferSize size)
        {
            frequency = (int)(1f / Time.fixedUnscaledDeltaTime);
            int sampleCount = TimeToSampleCount(size);
            Debug.Log("Set sample buffer to record " + size + " using " + sampleCount + " samples at " + frequency + "Hz");

            MaxSamples = sampleCount;
            DefaultBufferSize = size;

            samples = new SampleBuffer<byte[][]>(sampleCount);
            currentChunkID = 0;
        }

        void FixedRewindUpdate()
        {
            // If there are no more samples, stop the rewind.
            // TODO: Read in any previously recorded state from the hard disk, if streaming to storage is implemented.
            if (samples.Count == 0)
                return;

            RecordableMetadata meta;
            int sampleIdx = samples.Count - 1;

            // Set the state of all objects before the sample is discarded
            foreach (object o in registeredObjects.Keys)
            {
                registeredObjects.TryGetValue(o, out meta);

                SetState(o, sampleIdx);
            }

            // Remove the current sample from the list
            // If there is only one sample left, leave it so the state is set repeatedly.
            if (samples.Count > 1)
            {
                int sampleSize;
                sampleSize = BitConverter.ToInt32(samples[CurrentSample][0], 4);
                MemoryUsage -= sampleSize;
                samples.RemoveLast();
                absoluteSampleIdx--;
            }
        }

        /// <summary>
        /// Records the current state of all objects, modifying any with different time states.
        /// </summary>
        void FixedPlayUpdate()
        {
            RecordableMetadata meta;
            byte[][] newSample = CreateSample();
            int numBytes = 0;
            List<TimeBehaviour> behaviours;
            foreach (object o in registeredObjects.Keys)
            {
                registeredObjects.TryGetValue(o, out meta);

                if(behaviourSystems.TryGetValue(o.GetType(), out behaviours))
                {
                    foreach(TimeBehaviour tb in behaviours)
                    {
                        tb.OnFixedUpdate(o);
                    }
                }

                UpdateState(o);

                numBytes += RecordState(o, newSample);
            }
            SetSampleByteSize(newSample, numBytes);
            MemoryUsage += numBytes;
            AddSample(newSample);
        }

        void AddSample(byte[][] newSample)
        {
            samples.Add(newSample);
            absoluteSampleIdx++;

            // If we hit the sample cap, switch the buffers and write them out to disk
            if (samples.Count >= MaxSamples)
            {
                byte[] bytes = SampleBitConverter.SampleToBytes(CurrentChunkFileIdentifier, samples.SecondaryBuffer);
                ChunkStorer.StoreChunkAsync(bytes, CurrentChunkFileIdentifier);
                samples.RightShift();
                Debug.Log("Buffer full. Wrote chunk " + currentChunkID + " with " + bytes.Length + " bytes to disk at " + Application.persistentDataPath);

                currentChunkID++;
            }
        }
        
        /// <summary>
        /// Sets the time flow state for all registered objects.
        /// </summary>
        /// <param name="state"></param>
        public void SetTimeState(TimeState state)
        {
            foreach(object o in registeredObjects.Keys)
            {
                SetTimeState(o, state);
            }
        }

        /// <summary>
        /// Sets the time flow state for all registered objects, excluding any given.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="exclusions"></param>
        public void SetTimeState(TimeState state, params object[] exclusions)
        {
            // Add exclusions to a hashset to reduce the time complexity from O(mn) to O(m+n)
            HashSet<object> exclusionSet = new HashSet<object>(exclusions);

            foreach (object o in registeredObjects.Keys)
            {
                if(!exclusionSet.Contains(o))
                    SetTimeState(o, state);
            }
        }

        /// <summary>
        /// Sets the state of the given object, if it is registered.
        /// </summary>
        /// <param name="state">The time state the object will be set to.</param>
        public void SetTimeState(object obj, TimeState state)
        {
            //Debug.Log("Set time state to " + state + " for object " + obj);
            if (!registeredObjects.ContainsKey(obj))
                throw new ArgumentException("The object " + obj + " is not registered.");

            var meta = GetMetadata(obj);
            TimeState oldState = meta.state;
            meta.state = state;

            // Get all the behaviour systems for this object and update them,
            // if any exist.
            List<TimeBehaviour> handlers;
            if (behaviourSystems.TryGetValue(obj.GetType(), out handlers))
            {
                foreach (TimeBehaviour behaviour in handlers)
                {
                    behaviour.OnStateChanged(state, oldState, obj);
                }
            }
            else
            {
                //Debug.Log("No event handlers for " + obj);
            }

            switch (state)
            {
                case TimeState.NOMINAL:
                case TimeState.SLOW:
                case TimeState.FAST_FORWARD:
                case TimeState.REWIND:
                    meta.jumpFrameIdx = CurrentSample;
                    break;
                case TimeState.PLAYBACK:
                    break;
            }

        }

        /// <summary>
        /// Creates a new sample, formatting the header.
        /// </summary>
        /// <returns></returns>
        byte[][] CreateSample()
        {
            byte[][] result = new byte[registeredObjects.Count + 1][];
            // Make the header
            // 0-3: Int32 representing the number of objects in the sample
            byte[] sampleSize = BitConverter.GetBytes(registeredObjects.Count);

            result[0] = new byte[]
            {
                // Size of the sample (number of registered objects)
                sampleSize[0], sampleSize[1], sampleSize[2], sampleSize[3],
                // Size of sample (in total bytes, not counting the header)
                0, 0, 0, 0
            };
            return result;
        }
        
        /// <summary>
        /// Sets the "Byte Size" attribute of the given sample.
        /// This attribute describes the number of bytes stored in the sample.
        /// </summary>
        void SetSampleByteSize(byte[][] sample, int val)
        {
            byte[] asBytes = BitConverter.GetBytes(val);
            for(int ii = 0; ii < 4; ii++)
            {
                sample[0][4 + ii] = asBytes[ii];
            }
        }

        /// <summary>
        /// Updates the state of the object, if it needs it.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="meta"></param>
        void UpdateState(object o)
        {
            RecordableMetadata meta = GetMetadata(o);
            
            switch (meta.state)
            {
                case TimeState.NOMINAL:
                case TimeState.SLOW:
                case TimeState.FAST_FORWARD:
                case TimeState.PAUSE:
                    meta.jumpFrameIdx++;
                    break;
                case TimeState.REWIND:
                    SetState(o, meta.jumpFrameIdx);
                    //Debug.Log("Set state of " + o + " to frame " + meta.jumpFrameIdx);
                    meta.jumpFrameIdx--;
                    if(meta.jumpFrameIdx < 0)
                    {
                        meta.jumpFrameIdx = 0;
                    }
                    break;
                case TimeState.PLAYBACK:
                    // Set the state, advance the jump frame
                    SetState(o, meta.jumpFrameIdx);
                    meta.jumpFrameIdx++;
                    // ensure the frame index never goes out of range.
                    // once this happens, the state is set to nominal
                    if (meta.jumpFrameIdx >= samples.Count)
                    {
                        SetTimeState(o, TimeState.NOMINAL);
                    }
                    break;
                default:
                    throw new InvalidProgramException("Invalid time flow state: " + meta.state);
            }

        }

        /// <summary>
        /// Sets the state of an object to the state it had in the given sample index.
        /// If there was no recording data for the given sample, or the sample index is
        /// out of range, the state will not be set.
        /// Returns true if the state was set.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="sampleIdx"></param>
        bool SetState(object o, int sampleIdx)
        {
            RecordableMetadata meta = GetMetadata(o);
            StateDefinition def = GetDefinition(o.GetType());
            byte[][] sample = samples[sampleIdx];

            if (meta.id < sample.Length)
            {
                def.SetState(o, sample[meta.id]);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the state of the object and records it into the given sample.
        /// Returns the number of bytes that were recorded.
        /// </summary>
        /// <param name="o"></param>
        int RecordState(object o, byte[][] sample)
        {
            RecordableMetadata meta = GetMetadata(o);
            StateDefinition def;
            definitions.TryGetValue(o.GetType(), out def);
            sample[meta.id] = def.GetState(o);
            return sample[meta.id].Length;
        }

        /// <summary>
        /// O(1). Returns the metadata for the given object, o.
        /// Throws ArgumentException if it is not registered.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        RecordableMetadata GetMetadata(object o)
        {
            RecordableMetadata meta;
            if (registeredObjects.TryGetValue(o, out meta))
            {
                return meta;
            }
            throw new ArgumentException("The given object '" + o.ToString() + "' is not registered. Hashcode: " + o.GetHashCode());
        }

        /// <summary>
        /// O(1). Returns the definition for the given type, t.
        /// Throws ArgumentException if it has no definition.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        StateDefinition GetDefinition(Type t)
        {
            StateDefinition def;
            if(definitions.TryGetValue(t, out def))
                return def;

            throw new ArgumentException("The given type '" + t + "' has no definition.");
        }

        static int TimeToSampleCount(TEBufferSize time)
        {
            return frequency * (int)time;
        }
    }
}